#!/bin/bash

### Usage Information
# ./clean.sh
# Removes all files related to the group signature scheme in the current directory
###

rm -rf comm/ manager/ users/ signatures/
