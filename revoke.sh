#!/bin/bash

### Usage Information
# ./revoke.sh uid
# Revokes all currently actives certificates for user uid
####

TIMEFORMAT='%R'
uid=$1

echo -n "manager:revoke:" >> logs/time.log
{ time {
	for output in $(grep $uid manager/users.list | cut -f2 -d":")
	do
		openssl ca -config config/signing_master.cnf -revoke manager/cert_store/$output.pem -keyfile manager/root.sec -cert manager/root.crt
		if ! [ $? -eq 0 ]; then
			echo "Failed to revoke certificate $output"
		else
			rm manager/cert_store/$output.pem
		fi
	done

	# Removing the user from the users.list
	mv manager/users.list manager/users.list.old
	grep -v $uid manager/users.list.old > manager/users.list
	rm manager/users.list.old

	# Recreating the CRL
	openssl ca -config config/signing_master.cnf -gencrl -keyfile manager/root.sec -cert manager/root.crt -out manager/root.crl
} 2>/dev/null; } 2> >(tee -a manager/time.log logs/time.log >/dev/null)
