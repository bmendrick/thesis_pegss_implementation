#!/bin/bash

### Usage Information
# ./run_tests.sh
# Helpful script to create and revoke a set number of users automatically
###

TIMEFORMAT='%R'

cd "../$(dirname "$0")"

# Gathering run time data
echo -n "Desired lambda value? "
read lambda
export PEGSS_LAMBDA=$lambda

echo -n "How many members? "
read members

echo -n "How many revocations? "
read revokes

# Generating a group for further testing
./gen_group.sh 2> /dev/null

# Testing user creation
echo -n "Creating $members users..."
for (( i=1; i<=$members; i++ ))
do
    ./create_user.sh > /dev/null
done
echo " done!"

# Testing user revocation
echo -n "Revoking $revokes users..."
for output in $(ls users | tail -n $revokes)
do
    ./revoke.sh $output > /dev/null
done
echo " done!"

echo "NOTE: workspace is not cleaned after this test is run!"
