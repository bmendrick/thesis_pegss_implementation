#!/usr/local/bin/python3.8

### Usage Information
# ./compute_logs.py <time_log> <data_log>
# Calculates the average computation times listed in the time_log
# Calculates the amount of data sent between the user(s) and admin from the data_log
###

import os
import subprocess
import sys

# Checking that a test size was specified
if len(sys.argv) < 3:
    print("Please specify the log files: ./compute_logs.py <time.log> <data.log>")
    sys.exit(1)

record = {
    "generate": [],
    "keygen": [],
    "certsign": [],
    "certverify": [],
    "sign": [],
    "verify": [],
    "update": [],
    "open": [],
    "revoke": [],
    "managersend": [],
    "usersend": []
}

# Reading and parsing the contents of the time log file
with open(sys.argv[1], "r") as f:
    for line in f:
        args = line.strip().split(":")
        # GK
        if args[1] == "generate":
            record["generate"].append(float(args[2]))

        # Join
        elif args[1] == "keygen":
            record["keygen"].append(float(args[2]))

        elif args[1] == "certsign":
            record["certsign"].append(float(args[2]))

        elif args[1] == "certverify":
            record["certverify"].append(float(args[2]))

        # Sign
        elif args[1] == "sign":
            record["sign"].append(float(args[2]))

        # Verify
        elif args[1] == "certstatus":
            record["verify"].append(float(args[2]))

        elif args[1] == "signverify":
            record["verify"].append(float(args[2]))

        # Update
        elif args[1] == "updkeygen":
            record["update"].append(float(args[2]))

        elif args[1] == "updcertsign":
            record["update"].append(float(args[2]))

        elif args[1] == "updcertverify":
            record["update"].append(float(args[2]))

        # Open
        elif args[1] == "opencertverify":
            record["open"].append(float(args[2]))

        elif args[1] == "openverify":
            record["open"].append(float(args[2]))

        elif args[1] == "opendecrypt":
            record["open"].append(float(args[2]))

        # Revoke
        elif args[1] == "revoke":
            record["revoke"].append(float(args[2]))

        # Error
        else:
            print("ERROR: Unknown log line: " + line)

# Reading and parsing the contents of the data log file
with open(sys.argv[2], "r") as f:
    for line in f:
        args = line.strip().split(":")
        if args[0] == "manager":
            record["managersend"].append(int(args[3]))
        else:
            record["usersend"].append(int(args[3]))

# Computing the final values and displaying information to the user
print("Averages computed over {} trials".format(len(record["generate"])))

print("Avg time for group generations: {0:.3f} sec".format(
    sum(record["generate"]) / float(len(record["generate"]))
))

print("Avg time for new member key generation: {0:.3f} sec".format(
    sum(record["keygen"]) / float(len(record["keygen"]))
))

print("Avg time for manager to sign new member certificates: {0:.3f} sec".format(
    sum(record["certsign"]) / float(len(record["certsign"]))
))

print("Avg time for new member to verify their certificates: {0:.3f} sec".format(
    sum(record["certverify"]) / float(len(record["certverify"]))
))

print("Avg total time for a new member to join: {0:.3f} sec".format(
     (sum(record["keygen"]) + sum(record["certsign"]) + sum(record["certverify"])) / float(len(record["keygen"]))
))

print("Avg time to create a signature: {0:.3f} sec".format(
    sum(record["sign"]) / float(len(record["sign"]))
))

print("Avg time to verify a signature: {0:.3f} sec".format(
    sum(record["verify"]) / float(len(record["verify"]))
))

print("Avg time for new certificate generation during an update: {0:.3f} sec".format(
    sum(record["update"]) / float(len(record["updaterenew"]))
))

print("Avg time to open a signature: {0:.3f} sec".format(
    sum(record["open"]) / float(len(record["open"]))
))

print("Avg time to revoke a user: {0:.3f} sec".format(
    sum(record["revoke"]) / float(len(record["revoke"]))
))

print("Number of bytes sent from the manager to a user: {} bytes".format(sum(record["managersend"])))

print("Number of bytes sent from a user to the manager: {} bytes".format(sum(record["usersend"])))
