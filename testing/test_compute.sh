#!/bin/bash

### Usage Information
# ./run_tests.sh
# Script to test user creation, signatures, verifications, openings, updates, and revocations for a set number of iterations
###

TIMEFORMAT='%R'

cd "../$(dirname "$0")"

# Gathering run time data
echo -n "Desired lambda value? "
read lambda
export PEGSS_LAMBDA=$lambda

echo -n "How many iterations should be run? "
read iters

# Testing group generation
echo -n "Testing group generation..."
for (( i=1; i<=$iters; i++ ))
do
    echo -n "manager:generate:" >> logs/time.log
    { { time ./gen_group.sh 2> /dev/null; }; } 2>> logs/time.log
    ./clean.sh
done
echo " done!"

# Generating a group for further testing
./gen_group.sh 2> /dev/null

# Testing user creation
echo -n "Testing user creation..."
for (( i=1; i<=$iters; i++ ))
do
    ./create_user.sh > /dev/null
done
echo " done!"

# Testing signature creation
echo -n "Testing signature creation..."
for output in $(ls users)
do
    ./sign.sh $output samples/medium.jpg > /dev/null
done
echo " done!"

# Testing signature verification
echo -n "Testing signature verification..."
for output in $(cd signatures; echo */; cd ..)
do
    ./verify.sh signatures/$output/pegss.sig samples/medium.jpg > /dev/null
done
echo " done!"

# Testing signature opening
echo -n "Testing signature opening..."
for output in $(cd signatures; echo */; cd ..)
do
    ./open.sh signatures/$output/pegss.sig samples/medium.jpg > /dev/null
done
echo " done!"

# Testing user updates
echo -n "Testing user updates..."
for output in $(ls users)
do
    ./update.sh $output > /dev/null
done
echo " done!"

# Testing user revocation
echo -n "Testing user revocations..."
for output in $(ls users)
do
    ./revoke.sh $output > /dev/null
done
echo " done!"

# Cleaning up the workspace
./clean.sh
