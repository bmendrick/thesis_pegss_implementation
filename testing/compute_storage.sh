#!/bin/bash

### Usage Information
# ./compute_storage.sh <uid>
# Computes the private and non-private storage requirements for the given user (uid) and the manager
###

TIMEFORMAT='%R'
uid=$1

cd "../$(dirname "$0")"

echo -n "Amount of private user data (bytes): "
stat -f%z users/$uid/*.sec users/$uid/activekeys | paste -sd+ - | bc

echo -n "Amount of non-private user data (bytes): "
stat -f%z users/$uid/*.crt | paste -sd+ - | bc

echo -n "Amount of private manager data (bytes): "
stat -f%z manager/certindex* manager/certserial* manager/encrypt.key manager/nextuserid manager/root.sec manager/users.list | paste -sd+ - | bc

echo -n "Amount of non-private manager data (bytes): "
stat -f%z manager/cert_store/* manager/root.crl manager/root.crt | paste -sd+ - | bc
