#!/bin/bash

### Usage Information
# ./reset_workspace.sh
# Removes all files related to the group and resets the log files
###

cd "../$(dirname "$0")"
./clean.sh
./clean_logs.sh
