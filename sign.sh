#!/bin/bash

### Usage Information
# ./sign.sh uid file-to-sign
# Takes an unused key from user uid and uses it to sign file-to-sign
# Output is placed in sigantures folder indexed by certSerial-file
###

TIMEFORMAT='%R'
uid=$1
subject=$2

echo -n "$uid:sign:" >> logs/time.log
{ time {
# Determining what signing key will be used
key=$(tail -n1 users/$uid/activekeys)
dd if=/dev/null of=users/$uid/activekeys bs=1 seek=$(echo $(stat -f%z users/$uid/activekeys ) - $( tail -n1 users/$uid/activekeys | wc -c) | bc )
serial=$(openssl x509 -noout -serial -in users/$uid/$key.crt | cut -f2 -d"=")

# Bookkeeping for the signature
base=$(basename $subject | cut -f1 -d".")
mkdir signatures/$serial-$base

# Signing!
openssl dgst -sha256 -sign users/$uid/$key.sec $subject > signatures/$serial-$base/pegss.sig

# Moving the appropriate files
mv users/$uid/$key.crt signatures/$serial-$base/member.crt
cp manager/root.crt signatures/$serial-$base/
rm users/$uid/$key.sec
} 2>/dev/null; } 2> >(tee -a users/$uid/time.log logs/time.log >/dev/null)
