#!/bin/bash

### Usage Information
# ./update.sh uid
# Updates an existing user's certificates with a set of new certificates
###

TIMEFORMAT='%R'
uid=$1

# Generating the keys
echo -n "Removing old keys and generating new..."
echo -n "$uid:updkeygen:" >> logs/time.log
{ time {
	rm -f users/$uid/*.{crt,sec}
	> users/$uid/activekeys
	./cu_gen.sh $uid
} 2>/dev/null; } 2> >(tee -a users/$uid/time.log logs/time.log >/dev/null)
echo " done!"

# Making the certificates
echo -n "Signing certificates..."
echo -n "manager:updcertsign:" >> logs/time.log
{ { time ./cu_sign.sh $uid 2> /dev/null; }; } 2> >(tee -a manager/time.log logs/time.log >/dev/null)
echo " done!"

# Client retrieving certs and verifying them
echo -n "Verifying certificates..."
echo -n "$uid:updcertverify:" >> logs/time.log
{ { time ./cu_verify.sh $uid 2> /dev/null; }; } 2> >(tee -a users/$uid/time.log logs/time.log >/dev/null)
echo " done!"
