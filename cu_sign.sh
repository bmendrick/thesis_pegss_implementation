#!/bin/bash

### Usage Information
# ./cu_sign.sh uid
# Helper script for create_user.sh
# Retrieves CSRs for user uid from comm, generates correspondings certs, and places the certs back into comm
###

uid=$1

# Creating the user signing config file
cp config/user_master.cnf manager/tmp/user.cnf
echo -n "dummy" >> manager/tmp/user.cnf

# Retrieving the user csr files
mv comm/$uid-*.csr manager/tmp/csr

for output in $(ls manager/tmp/csr)
do
	# Changing output to not have a file extension
	output=$(echo $output | cut -f1 -d".")

	# Remove last line of config
	dd if=/dev/null of=manager/tmp/user.cnf bs=1 seek=$(echo $(stat -f%z manager/tmp/user.cnf ) - $( tail -n1 manager/tmp/user.cnf | wc -c) | bc )

	# Manager generating mu for the user
	echo $uid  > manager/tmp/uid
	openssl enc -chacha -a -k manager/encrypt.key -md sha256 -in manager/tmp/uid -out manager/tmp/uid.enc

	# Updating config with encrypted data
	echo 1.2.3.4=ASN1:UTF8String:$(cat manager/tmp/uid.enc) >> manager/tmp/user.cnf

	# Manager satisfying the csr
	serial=$(cat manager/certserial)
	openssl ca -batch -config config/signing_master.cnf -notext -in manager/tmp/csr/$output.csr -out manager/tmp/crt/$output.crt -extfile manager/tmp/user.cnf -extensions v3_req
	echo "$uid:$serial" >> manager/users.list
done

# Cleaning manager files
rm manager/tmp/user.cnf manager/tmp/uid* manager/tmp/csr/*

# Manager sending the certificate back to the member
for output in $(ls manager/tmp/crt)
do
	mv manager/tmp/crt/$output comm/
	echo "manager:$uid:$output:$(stat -f%z comm/$output)" | tee -a comm/data.log logs/data.log >/dev/null
done
