#!/bin/bash

### Usage Information
# ./cu_verify.sh uid
# Helper script for create_user.sh
# Retrieves user uid's certificates from comm and verifies they generated correctly
###

uid=$1

for output in $(cat users/$uid/activekeys)
do
	# Retrieve the cert
	mv comm/$output.crt users/$uid/

	# Checking the certificate and key match
	check1=$(openssl x509 -in users/$uid/$output.crt -pubkey -noout -outform PEM | sha256sum)
	check2=$(openssl pkey -in users/$uid/$output.sec -pubout -outform PEM | sha256sum)
	if [ "$check1" != "$check2" ]; then
		echo Client failed to verify certificate users/$uid/$output.crt
	fi
done
