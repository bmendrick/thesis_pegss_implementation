#/bin/bash

### Usage Information
# ./open.sh signature-file file-to-sign
# Verifies the signature-file is valid for file-to-sign and then retrieves the user ID from the certificate
# Note: does not compare the certificate to a CRL when verifying
###

TIMEFORMAT='%R'
sig=$1
subject=$2

path=$(dirname $sig)
result=0

# Verifying that the certificate is valid (note, not comparing to CRL)
echo -n "manager:opencertverify:" >> logs/time.log
{ time {
	openssl verify -CAfile $path/root.crt $path/member.crt
	result=$?
}; 2>/dev/null; } > /dev/null 2> >(tee -a manager/time.log logs/time.log > /dev/null)
if ! [ $result -eq 0 ]; then
	echo "Failed to verify certificate!"
	exit 1
fi

# Verifying the signature on the data is real
echo -n "manager:openverify:" >> logs/time.log
{ time {
	openssl x509 -in $path/member.crt -pubkey -noout -outform PEM > $path/member.pub
	openssl dgst -sha256 -verify $path/member.pub -signature $sig $subject
	result=$?
}; 2>/dev/null; } 1>/dev/null 2> >(tee -a manager/time.log logs/time.log > /dev/null)
if ! [ $result -eq 0 ]; then
	echo "Failed to verify signature!"
	exit 1
fi

# Pulling out the member info and decrypting it
echo -n "manager:opendecrypt:" >> logs/time.log
{ time {
	openssl x509 -in $path/member.crt -text -noout | grep -i -A 1 '1.2.3.4' | tail -n 1 | sed -e 's/^[[:space:]\.$]*//' > $path/uid.enc
	openssl enc -chacha -d -a -k manager/encrypt.key -md sha256 -in $path/uid.enc -out $path/uid
	result=$?
}; 2>/dev/null; } 2> >(tee -a manager/time.log logs/time.log > /dev/null)
if ! [ $result -eq 0 ]; then
	echo "Failed to decrypt the user ID!"
	exit 1
fi

cat $path/uid
