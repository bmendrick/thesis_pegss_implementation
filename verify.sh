#!/bin/bash

### Usage Information
# ./verify.sh signature-file file-to-sign
# Verifies the signature signature-file is valid for the message/file file-to-sign
# Uses a stored version of the root.crt, but "retrieves" an up-to-date version of the CRL
###

TIMEFORMAT='%R'
sig=$1
subject=$2

path=$(dirname $sig)
result=0

# Checking the certificate status
echo -n "verifier:certstatus:" >> logs/time.log
{ time {
	openssl verify -crl_check -CAfile <(cat $path/root.crt manager/root.crl) $path/member.crt
	result=$?
}; 2>/dev/null; } 2> >(tee -a signatures/time.log logs/time.log >/dev/null)
if ! [ $result -eq 0 ]; then
	echo "Failed to verify certificate!"
	exit 1
fi

# Checking the signature
echo -n "verifier:signverify:" >> logs/time.log
{ time {
	openssl x509 -in $path/member.crt -pubkey -noout -outform PEM > $path/member.pub
	openssl dgst -sha256 -verify $path/member.pub -signature $sig $subject
	result=$?
}; 2>/dev/null; } 2> >(tee -a signatures/time.log logs/time.log >/dev/null)
if ! [ $result -eq 0 ]; then
	echo "Failed to verify signature!"
	exit 1
fi
