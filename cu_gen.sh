#!/bin/bash

### Usage Information
# ./cu_gen.sh uid
# Helper script for create_user.sh
# Generates lambda keys and their corresponding CSRs for user uid and puts them in comm
###

TIMEFORMAT='%R'
uid=$1

basekeyid=$(cat users/$uid/basekeyid)

# Generating lambda keys
for (( i=1; i<=$PEGSS_LAMBDA; i++ ))
do
	base=$(printf "%s-%03d" $uid $(($i + $basekeyid)))
	openssl ecparam -noout -genkey -name prime256v1 -outform PEM -out users/$uid/$base.sec
	openssl req -new -key users/$uid/$base.sec -out users/$uid/csr/$base.csr -config config/user_master.cnf
	echo "$base" >> users/$uid/activekeys
done

# Tracking key id base
echo $(($PEGSS_LAMBDA + $basekeyid)) > users/$uid/basekeyid

# Sending csr to manager
for output in $(ls users/$uid/csr)
do
	mv users/$uid/csr/$output comm/
	echo "$uid:manager:$output:$(stat -f%z comm/$output)" | tee -a comm/data.log logs/data.log >/dev/null
done
