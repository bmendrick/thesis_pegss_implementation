#!/bin/bash

### Usage Information
# ./create_user.sh
# Creates a new user as part of the gss setup in the current directory
###

TIMEFORMAT='%R'

# Choosing and updating member id
uid=$(printf "%08d" $(cat manager/nextuserid))
echo $(($(cat manager/nextuserid) + 1)) > manager/nextuserid
echo "Creating user $uid"

# Creating/Establishing the member
mkdir users/$uid
mkdir users/$uid/csr
touch users/$uid/{time.log,activekeys}
echo 0 > users/$uid/basekeyid

# Generating the keys
echo -n "Generating keys..."
echo -n "$uid:keygen:" >> logs/time.log
{ time { ./cu_gen.sh $uid 2> /dev/null; }; } 2> >(tee -a users/$uid/time.log logs/time.log >/dev/null)
echo " done!"

# Making the certificates
echo -n "Signing certificates..."
echo -n "manager:certsign:" >> logs/time.log
{ { time ./cu_sign.sh $uid 2> /dev/null; }; } 2> >(tee -a manager/time.log logs/time.log >/dev/null)
echo " done!"

# Client retrieving certs and verifying them
echo -n "Verifying certificates..."
echo -n "$uid:certverify:" >> logs/time.log
{ { time ./cu_verify.sh $uid 2> /dev/null; }; } 2> >(tee -a users/$uid/time.log logs/time.log >/dev/null)
echo " done!"
