#!/bin/bash

### Usage Information
# . ./gen_group.sh
# ALT: source /gen_group.sh
# Sets up the structure of a group signature scheme in the current directory
###

# Making the folder structure
mkdir {manager,comm,users,signatures}
mkdir manager/{cert_store,tmp}
mkdir manager/tmp/{crt,csr}

export PEGSS_LAMBDA=100

# Making key files
touch manager/{certindex,users.list,time.log}
openssl rand -hex 8 | tr '[:lower:]' '[:upper:]' > manager/certserial
echo 1 > manager/nextuserid

touch comm/data.log
touch signatures/time.log

# Creating the manager's encryption key
openssl rand -hex -out manager/encrypt.key 32

# Creating the manager's signing key pair
openssl ecparam -noout -genkey -name prime256v1 -outform PEM -out manager/root.sec
openssl req -new -x509 -days 1826 -key manager/root.sec -out manager/root.crt -config config/root_master.cnf

# Creating the CRL
openssl ca -config config/signing_master.cnf -gencrl -keyfile manager/root.sec -cert manager/root.crt -out manager/root.crl
